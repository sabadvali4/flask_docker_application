from flask import Flask,render_template,redirect,url_for,request,flash
from flask_sqlalchemy import SQLAlchemy
import functions
import os
import json
import urllib.request



app = Flask(__name__,template_folder='./temblates')

BASE_FOLDER =  os.path.dirname(os.path.abspath(__file__))
RESOURCE_DIR = os.path.join(BASE_FOLDER,"node_dir")


app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///webuser_db.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)
app.secret_key = 'webapp'



class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), nullable=False)
    age = db.Column(db.Integer,nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)
    password = db.Column(db.String(120), unique=True, nullable=False)



logged_in = False

@app.route("/",methods=["GET","POST"])
def chose():

    if request.method == 'POST':
        if 'login_btn' in request.form:
            return redirect(url_for("login"))
        elif 'registration_btn' in request.form:
            return redirect(url_for("registration"))
        
    return render_template('both.html')



@app.route("/login",methods=["GET","POST"])
def login():
    msg = False
    if request.method == "POST":
        if 'back_btn' in request.form:
            return redirect(url_for("chose"))
        try:
            log_email = request.form['log_email_input']
            log_pswrd = request.form['log_password_input']
            eml = db.session.query(User.email).all()
            pswrd = db.session.query(User.password).all()
            checked = functions.login_check(log_email,log_pswrd,eml,pswrd)
            if checked == 1:
                usn = db.session.query(User).filter(User.email == log_email).first()
                usn = usn.name
                global logged_in
                logged_in = True  
                return redirect(url_for("mainboard",usn=usn))
                
            elif checked == -1:
                print("kii")
                msg = "Email Or Password does not match"
                return render_template("login.html",msg=msg)
        except:
            return "sorry database error try again"
        
    return render_template("login.html",msg=msg)



@app.route("/Registration",methods=["GET","POST"])
def registration():
    if request.method == "POST":
        if 'back_btn' in request.form:
            return redirect(url_for("chose"))  
        name1 = request.form['reg_name_input']
        age1 = request.form['reg_age_input'] 
        email1 = request.form['reg_email_input']
        password1 = request.form['reg_password_input']
        repassword = request.form['reg_re_password_input']
        if len(name1) == 0 or age1 == 0 or len(email1) == 0 or len(password1) == 0 or len(repassword) == 0:
            flash("enter all values")
            return redirect(url_for("registration"))
        age1 = int(age1)
        if age1 < 18:
            flash("you are not a legal age")
            return redirect(url_for("registration"))

        eml = db.session.query(User.email).all()
        for i in eml:
            if i[0] == email1:
               flash("this email is already registered")
               return redirect(url_for("registration"))
            
        checkedP = functions.checkP(password1,repassword)
        if checkedP == -1:
            flash("Password does not match")
            return redirect(url_for("registration"))
        elif checkedP == -2:
            flash("Password must contain minimum 8 and maxsimum 20 characters")
            return redirect(url_for("registration"))
        elif checkedP == 1:
            try:
                insert = User(name=name1,age=age1,email=email1,password=password1)
                db.session.add(insert)
                db.session.commit()
                global logged_in 
                logged_in = True     
                return redirect(url_for("mainboard",usn=name1))      
            except:
                return "There is problem on DataBase Try again later."
            
    return render_template("registration.html")



@app.route("/mainboard/<usn>",methods=["POST","GET"])
def mainboard(usn):
    global logged_in
    if logged_in == True:
        json_url = "http://4.204.222.134:8000/"
        if request.method == "POST":
            
            if 'btn_back' in request.form:
                with urllib.request.urlopen(json_url) as json_url:
                    data = json.loads(json_url.read().decode())    
                # with open(os.path.join(RESOURCE_DIR,"data.json"))as f:
                #     data = (json.loads(f.read()).get("fruits"))
                    return render_template("main.html",data=data,usn=usn) 
            tb = db.session.query(User).all()
            print(tb)
            return render_template("db.html", tb = tb)  
              
        with urllib.request.urlopen(json_url) as json_url:
            data = json.loads(json_url.read().decode()) 
        # with open(os.path.join(RESOURCE_DIR,"data.json"))as f:
        #     data = (json.loads(f.read()).get("fruits"))
            return render_template("main.html",data=data,usn=usn)   
    elif logged_in == False:
        return redirect(url_for("chose"))




if __name__ == "__main__":
    with app.app_context():
        db.create_all()
    app.run(debug=True,host='0.0.0.0',port=5555)
