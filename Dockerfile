FROM python:3.7

RUN mkdir /app

WORKDIR /app/

COPY . /app/

RUN pip install --no-cache-dir -r requirements-prod.txt

EXPOSE 5555

CMD ["python", "app.py"]
